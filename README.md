# No Dunning Kruger

A toy notebook demonstrating the suggested (as in [this blog-post on mcgill.ca](https://www.mcgill.ca/oss/article/critical-thinking/dunning-kruger-effect-probably-not-real)) appearance of the Dunning-Kruger graph not from human bias but from statistics alone.
